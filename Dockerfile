FROM mariadb

RUN apt-get update && apt-get -y install nano  bash iputils-ping dnsutils avahi-daemon avahi-utils && echo "[server]\nuse-ipv4=yes\nuse-ipv6=yes\nratelimit-interval-usec=1000000\nratelimit-burst=1000\n[wide-area]\nenable-wide-area=yes\n[publish]\npublish-hinfo=no\npublish-workstation=yes\n" > /etc/avahi/avahi-daemon.conf

# sed 's/\(\|\#\)publish-\(workstation\|addresses\)=no/\0=yes/g' /etc/avahi/avahi-daemon.conf -i && 

COPY spawn_avahi.sh /
# INJECTION INTO ORIGINAL ENTRYPOINT
RUN chmod +x /spawn_avahi.sh
#ENTRYPOINT ["/bin/bash","/spawn_avahi.sh"]
RUN mv /usr/local/bin/docker-entrypoint.sh /usr/local/bin/docker-entrypoint-real.sh 
RUN ln -s /spawn_avahi.sh /usr/local/bin/docker-entrypoint.sh  
RUN chmod +x /spawn_avahi.sh
RUN sed 's/#!\/bin\/bash/\0\n\necho INIT\n\/spawn_avahi.sh &\n/g' /usr/local/bin/docker-entrypoint.sh -i && chmod +x /spawn_avahi.sh
#ENTRYPOINT ["/bin/bash","/usr/local/bin/docker-entrypoint.sh","mysqld","--user=root"]
#ENTRYPOINT ["/bin/bash","-c"," /bin/bash /spawn_avahi.sh &  /usr/local/bin/docker-entrypoint.sh "]
CMD ["/bin/bash","/usr/local/bin/docker-entrypoint.sh","mysqld","--user=root"]
